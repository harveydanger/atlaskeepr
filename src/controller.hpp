//
//  controller.hpp
//  atlas_keepr
//
//  Created by Vladimir Sukhov on 10/8/18.
//  Copyright © 2018 Vladimir Sukhov. All rights reserved.
//

#ifndef controller_hpp
#define controller_hpp

#define CBC 1 // Encryption type

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <math.h>
#include <string.h>
#include <pwd.h>
#include <ctime> // for pseudo random

#include "vault.hpp"
#include "fs_inter.hpp"
#include "arg_parser.hpp"

#include <algorithm>
#include <iterator>

#include "aes.hpp"
#include "macro.h"



#ifndef PASS_LEN
    #define PASS_LEN 16
#endif

#ifndef CHUNK
    #define CHUNK 64
#endif


using namespace std;

class Controller
{
private:
    
    string homeDir;
    Vault *vault;
    FSWriter *fs;
    struct reqs request;
    
    // Encryption
    struct AES_ctx ctx;
    uint8_t key[PASS_LEN] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                             0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
    
    uint8_t iv[PASS_LEN] = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                       0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f };
    
    string password = "";
    
    // Helper functions & accessors
    string getFullPath(string vaultName);
    string getCurrentHomeFolder();
    struct reqs *getRequest();
    
    // data consistency
    int dataLen = 0;
    uint8_t atlas_key[DELIMITER_LEN] = {0x3C, 0x41, 0x54, 0x4C, 0x41, 0x53, 0x3E }; // <ATLAS>
    const char characters[93] = "0123456789qazwsxedcrfvtgbyhnujmikolpQAZWSXEDCRFVTGBYHNUJMIKOLP!@#$%^&*()-_=+[]{}\\|;:',<.>/?~";
    
    // Argument delegation
    string *getStringArguments(int argc, const char *args[]);
    void delegateArguments(int argc, const char *argv[]);
    
    // Vault operations
    int createVault(string _name);
    int removeVault(string _vault_name);
    
    // Password control
    string retrievePassword();
    string generatePassword(int len);
    
    // Encryption - Decryption
    uint8_t *encrypt_uint(string row);
    string decrypt_uint(vector<uint8_t> data);
    void xcryptVector(vector<string> rows, string password, bool encrypt, vector<string> *dest);
    void strToIntKey(string key, uint8_t store[], int keyLen);
    vector<string> getVaultRows(string data);
    
    void mutateArray(uint8_t *src, uint8_t *dest, int src_size, int dest_size, int dest_start_pos);
    
public:
    
    Controller();
    int run(int argc, const char *argv[]);
};

#endif /* controller_hpp */
