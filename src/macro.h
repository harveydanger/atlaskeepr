//
//  macro.h
//  atlas_keepr
//
//  Created by Vladimir Sukhov on 10/8/18.
//  Copyright © 2018 Vladimir Sukhov. All rights reserved.
//

#ifndef macro_h
#define macro_h

#define OK_EXIT_CODE 0
#define ERROR_EXIT_CODE -1
#define NOT_FOUND "not found"
#define DEF_DIR ".atlaskeepr"

#define DELIMITER "<ATLAS>"
#define DELIMITER_LEN 7
#define ATLAS_PH "ATLASPH"
#define ATLAS_PREFIX "name: ATLASPH value: ATLASPH<ATLAS>"
#define ATLAS_SERVICE_ROW "name: ATLASPH value: ATLASPH"

#endif /* macro_h */
