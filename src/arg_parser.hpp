//
//  arg_parser.hpp
//  atlas_keepr
//
//  Created by Vladimir Sukhov on 11/8/18.
//  Copyright © 2018 Vladimir Sukhov. All rights reserved.
//

#ifndef arg_parser_hpp
#define arg_parser_hpp

#define DEFAULT_PASS_LEN 12

#include <stdio.h>
#include <string.h>
#include <iostream>


using namespace std;
typedef enum Request
{
    CREATE_VAULT, // -cv <vault name>
    LIST_VAULTS, // -l list all vaults
    REMOVE_VAULT, // -rm <vault_name>
    ADD_NODE, // -a <node name> -p <value> -v <vault name>
    GET_VALUE, // -g <node name> -v <vault name>
    REMOVE_NODE, // -rm <node name> -v <vault name>
    HELP, // -h
    GET_ALL_NODES // -all -v <vault name>
    
} request;

struct reqs {
    request type;
    string vaultName;
    string nodeName;
    string value;
    bool error;
    bool generate = false;
    int genLength = DEFAULT_PASS_LEN;
};

void ap_getRequestType(int argc, string argv[], struct reqs *_req_struct);
void ap_showHelp();


#endif /* arg_parser_hpp */
