//
//  fs_writer.cpp
//  atlas_keepr
//
//  Created by Vladimir Sukhov on 10/8/18.
//  Copyright © 2018 Vladimir Sukhov. All rights reserved.
//

#include "fs_inter.hpp"


FSWriter::FSWriter()
{
    
}

int FSWriter::createDefaultFolder(string _abs_path)
{
    int stat = mkdir(_abs_path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    
    if(!stat)
    {
        cout << "Atlas store has been created at " << _abs_path << endl;
        return OK_EXIT_CODE;
    }
    
    return ERROR_EXIT_CODE;
}

int FSWriter::createVault(string _abs_path)
{
    if(!this->fileExists(_abs_path))
    {
        ofstream outfile (_abs_path);
        return OK_EXIT_CODE;
    }
    return ERROR_EXIT_CODE;
}

int FSWriter::removeVault(string _abs_path)
{
    if(this->fileExists(_abs_path))
    {
        remove(_abs_path.c_str());
        return OK_EXIT_CODE;
    }
    else
    {
        return ERROR_EXIT_CODE;
    }
}


vector<uint8_t> FSWriter::readVault_uint(string _abs_path)
{
    vector<uint8_t> data;
    
    FILE *f = fopen(_abs_path.c_str(), "rb");
    if(!f)
    {
        cout << "Vault does not exist." << endl;
        exit(0);
    }
    
    while(!feof(f))
    {
        uint8_t byte;
        fread(&byte,1,1,f);
        data.push_back(byte);
    }
    
    data.pop_back(); // 1 redundant element from while loop
    return data;
}


void FSWriter::writeVault_uint(uint8_t *_data, int data_len, std::string _abs_path)
{
    this->removeVault(_abs_path);
    this->createVault(_abs_path);
    
    FILE *f = fopen(_abs_path.c_str(), "wb");
    if(!f)
    {
        cout << errno << ": CRITICAL ERROR OCCURED, TERMINATING." << endl;
        exit(0);
    }
    
    if(fwrite(_data,sizeof(unsigned char), data_len, f) == 0)
    {
        cout << errno << ": CRITICAL WRITE ERROR. TERMINATING." << endl;
        exit(0);
    }
    
    fclose(f);
    
}

bool FSWriter::dirExists(string _abs_path)
{
    struct stat info;
    
    if(stat( const_cast<char*>(_abs_path.c_str()), &info ) != 0)
    {
        return false;
    }
    else if(info.st_mode & S_IFDIR)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool FSWriter::fileExists(string _abs_path)
{
    struct stat buffer;
    return (stat (_abs_path.c_str(), &buffer) == 0);
}

int FSWriter::readDirContents (string dir, vector<string> &files)
{
    DIR *dp;
    struct dirent *dirp;
    if((dp  = opendir(dir.c_str())) == NULL) {
        cout << "Error(" << errno << ") opening " << dir << endl;
        return errno;
    }
    
    while ((dirp = readdir(dp)) != NULL) {
        files.push_back(string(dirp->d_name));
    }
    closedir(dp);
    return 0;
}


void FSWriter::listAllVaults(string _abs_path)
{
    string dir = _abs_path;
    vector<string> files = vector<string>();
    
    this->readDirContents(dir,files);
    
    for (unsigned int i = 0;i < files.size();i++)
    {
        if(files[i] != "." && files[i] != ".." && files[i][0] != '.')
        cout << files[i] << endl;
    }
}
